package br.com.itau.investimentocliente.controllers

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.ws.rs.core.MediaType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.test.web.servlet.MockMvc

import br.com.itau.investimentocliente.models.Cliente
import br.com.itau.investimentocliente.repositories.ClienteRepository
import br.com.itau.investimentocliente.services.ClienteService
import spock.lang.Specification
import spock.mock.DetachedMockFactory


@WebMvcTest
class ClienteControllerTest extends Specification{
	
	@Autowired
	MockMvc mockMvc;
	
	@Autowired
	ClienteService clienteService;
	
	@Autowired
	ClienteRepository clienterepository;
	
	 @TestConfiguration
	 static class MockCOnfig {
		 def factory = new DetachedMockFactory()
		 
		 @Bean
		 ClienteService clienteservice() {
			 return factory.Mock(ClienteService)
		 }
		 
		 @Bean
		 ClienteRepository clienterepository() {
			 return factory.Mock(ClienteRepository)
		 }
		 
	 }
	
	 def 'deve buscar um cliente por CPF' (){
		given: 'os dados do cliente exitem na base'
		String cpf = '12312312311'
		Cliente cliente = new Cliente()
		cliente.setNome('Liu')
		cliente.setCpf(cpf)
		Optional clienteOptional = Optional.of(cliente)
		
		when: 'a busca é realizada'
		def resposta = mockMvc.perform(get("/12312312311"));
		
		then: 'uma busca é realizada'
		1 * clienteService.buscar(_) >> clienteOptional
		clienteOptional.isPresent() == true
		
		
	 }
	 
	 def 'deve inserir um novo cliente' (){
		 given: 'os dados do cliente são informados'
		 Cliente cliente = new Cliente()
		 cliente.setNome('Liu')
		 cliente.setCpf('12312312311')
		 
		 when: 'um novo cadastro é realizado'
		 def resposta = mockMvc.perform(
			 post('/')
			 .contentType(MediaType.APPLICATION_JSON)
			 .content('{"nome": "Liu", "cpf": "12312312311"}'))
		 
		 then: 'insira um cliente corretamente'
		 1 * clienteService.cadastrar(_) >> cliente
		 resposta.andExpect(status().isCreated())
		 		 .andExpect(jsonPath('$.nome').value('Liu'))
		 
	  }
	
	
	
}
